import socket
import fcntl
import struct
import re

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]




from subprocess import Popen, PIPE
def get_mac():

    # do_ping(IP)
    # The time between ping and arp check must be small, as ARP may not cache long

    pid = Popen(["arp", "-n", IP], stdout=PIPE)
    s = pid.communicate()[0]
    mac = re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})", s).groups()[0]
    logger.info(mac)



