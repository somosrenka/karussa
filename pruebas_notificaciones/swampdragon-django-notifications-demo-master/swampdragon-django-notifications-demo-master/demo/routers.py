from swampdragon import route_handler
from swampdragon.message_format import format_message
from swampdragon.permissions import LoginRequired
from swampdragon.route_handler import ModelPubRouter, BaseRouter
from .models import Notification
from .serializers import NotificationSerializer

'''
class NotificationRouter(ModelPubRouter):
    valid_verbs = ['subscribe','get_single']
    #valid_verbs = ['subscribe']
    route_name = 'notifications'
    model = Notification
    serializer_class = NotificationSerializer

    def get_object(self, **kwargs):
        logger.info(self)
        logger.info(kwargs)
        pass
'''

from swampdragon.route_handler import ModelRouter
class TodoItemRouter(ModelRouter):
    valid_verbs = ['subscribe']
    route_name = 'todo-item'
    serializer_class = NotificationSerializer
    model = Notification
    permission_classes = [LoginRequired()]

    def get_object(self, **kwargs):
        return self.model.objects.get(pk=kwargs['id'])

    def get_query_set(self, **kwargs):
        return self.model.objects.filter(user__id=kwargs['list_id'])

#class NotificationRouter(ModelPubRouter):
#class NotificationRouter(ModelRouter):
class NotificationRouter(ModelPubRouter):
    valid_verbs = ['subscribe']
    #valid_verbs = ['subscribe','get_single']
    route_name = 'notifications'
    model = Notification
    serializer_class = NotificationSerializer

    def get_query_set(self, **kwargs):
        logger.info("get_query_set_________________-")
        logger.info(self)
        logger.info(kwargs)
        return self.model.objects.filter(user=self.connection.user)

    def get_object(self, **kwargs):
        logger.info("get_object____________________")
        logger.info(self)
        logger.info(kwargs)
        return self.model.objects.get(user=self.connection.user, pk=kwargs['pk'])

    def get_subscription_contexts(self, **kwargs):
        try:
            logger.info("get_subscription_contexts______________________")
            logger.info(self)
            logger.info(""+str(vars(self)))
            logger.info("conection.sesion "+str(vars(self.connection.session)))
            logger.info("conection.session_store,conection.pub_sub._subscriber "+str(vars(self.connection.session_store.connection.pub_sub._subscriber )))
            logger.info("self.conection.user "+ str(self.connection.user))
        except Exception as e:
            logger.info(e)
        return {'user__id': self.connection.user.pk}


    def get_client_context(self, verb, **kwargs):
        logger.info("get_client_context________________________")
        logger.info(self)
        logger.info(verb)
        logger.info(kwargs)
        logger.info("self "+str(vars(self)))
        logger.info("conection "+str(vars(self.connection)))
        logger.info("conection.sesion "+str(vars(self.connection.session)))
        logger.info("conection.user "+str(self.connection.user))
        logger.info("verb "+verb)
        return {'user__id': self.connection.user.pk}

    def send(self, data, channel_setup=None, **kwargs):
        logger.info("metodo send______________________-")
        self.context['state'] = 'success'
        if 'verb' in self.context:
            client_context = self.get_client_context(self.context['verb'], **kwargs)
            self._update_client_context(client_context)
            logger.info("Client_context : "+str(client_context))

        message = format_message(data=data, context=self.context, channel_setup=channel_setup)
        logger.info("se enviara el mensaje "+str(message))
        self.connection.send(message)





class NotificationRouter_2(BaseRouter):
    route_name = 'call-notifications'

    def get_subscription_channels(self,**kwargs):
        channel = 'user_{}'.format(self.connection.user.pk)  # This is depending on swampdragon-auth
        return

route_handler.register(NotificationRouter)
