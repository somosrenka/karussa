package vista;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.JFXPanel;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

public class JFrameKarussa extends JFrame
{
    private final JPanel panelPadre = new JPanel(new BorderLayout());
    private JFXPanelKarussa jfxPanelKarussa = JFXPanelKarussa.getInstance();
    //WebView web = new WebView();
    //WebEngine navegador = new WebEngine();
    //private JFXPanelKarussa jfxPanelKarussa = JFXPanelKarussa.getInstance(web, navegador);
    
    private JPopupMenu raizMenu;
    private JPopupMenu.Separator separador1;
    private JMenuItem maximizar;
    private JMenuItem salir;
    
    public JFrameKarussa()
        {
            super();
            initComponents();
        }

    private void initComponents() {
        raizMenu = new JPopupMenu();
        maximizar = new JMenuItem();
        separador1 = new JPopupMenu.Separator();
        salir = new JMenuItem();
        JPanel topBar = new JPanel(new BorderLayout(5, 0));

  //      maximizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg"))); // NOI18N
        maximizar.setText("jMenuItem1");
        
        raizMenu.add(maximizar);
        raizMenu.add(separador1);

        salir.setText("jMenuItem2");

        raizMenu.add(salir);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 600));


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );
        
        panelPadre.add(topBar, BorderLayout.NORTH);
        panelPadre.add(jfxPanelKarussa, BorderLayout.CENTER);

        getContentPane().add(panelPadre);

        setPreferredSize(new Dimension(1024, 600));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();

    }
    
   public JFXPanel getJFXPanel()
   {
       return this.jfxPanelKarussa;
   }




    public static void main(String args[]) 
    {

        /* Create and display the form */
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                JFrameKarussa objecto = new JFrameKarussa();
                
                String url = "https://www.somosrenka.com/";
                objecto.setVisible(true);
                objecto.jfxPanelKarussa.setURL(url);
                objecto.jfxPanelKarussa.loadURL(url);                
            }
        });

    }



}


