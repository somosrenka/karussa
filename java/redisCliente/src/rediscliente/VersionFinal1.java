package rediscliente;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nicon.notify.core.Notification;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import static javafx.concurrent.Worker.State.FAILED;

public class VersionFinal1 extends JFrame {

    private final JFXPanel jfxPanel = new JFXPanel();
    private WebEngine engine;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem maximizar;
    private javax.swing.JMenuItem salir;
    SystemTray st;
    TrayIcon ti;
    Image imagenIco;
    private final JPanel panel = new JPanel(new BorderLayout());

    public VersionFinal1() {
        initComponents();
        System.out.println("se cargaron los componentes");
        if (SystemTray.isSupported()) {
            st = SystemTray.getSystemTray();
            imagenIco = new ImageIcon(getClass().getResource("/iconos/iconito.png")).getImage();

            ti = new TrayIcon(imagenIco, "SystemTray", null);
            maximizar.setText("Maximizar");
            salir.setText("Salir");
            ti.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent evt) {
                    jPopupMenu1.setLocation(evt.getX(), evt.getY());
                    jPopupMenu1.setInvoker(jPopupMenu1);
                    jPopupMenu1.setVisible(true);
                }
            });
            ti.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    ti.displayMessage("MenuItem Maximizar", "MenuItem2 Salir", TrayIcon.MessageType.INFO);
                }
            });
            ti.setImageAutoSize(true);
        } else {
            JOptionPane.showMessageDialog(this, "no furula minimizar 2", "no furula minimizar 1", JOptionPane.OK_OPTION);
        }
        System.out.println("se cargo el system tray");
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        try {
            createScene();
        } catch (Exception e) {
            System.out.println(e.getCause());
            System.out.println(e.getClass());
        }

        panel.add(jfxPanel, BorderLayout.CENTER);
        getContentPane().add(panel);

        JPanel topBar = new JPanel(new BorderLayout(5, 0));
        topBar.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
        jPopupMenu1 = new javax.swing.JPopupMenu();
        maximizar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        salir = new javax.swing.JMenuItem();

        maximizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_open_in_new_black_24dp_1x.png"))); // NOI18N
        maximizar.setText("jMenuItem1");
        maximizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maximizarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(maximizar);
        jPopupMenu1.add(jSeparator1);

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_exit_to_app_black_24dp_1x.png"))); // NOI18N
        salir.setText("jMenuItem2");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(salir);
        //setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 403, Short.MAX_VALUE)
        );

        getContentPane().add(panel);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        pack();

    }

    private void maximizarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        setVisible(true);
        toFront();
        st.remove(ti);
    }

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        System.exit(0);
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {
        // TODO add your handling code here:
        setVisible(false);
        try {
            st.add(ti);
        } catch (AWTException ex) {
            Logger.getLogger(systemTray.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createScene() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                WebView view = new WebView();
                engine = view.getEngine();
                engine.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String oldValue, final String newValue) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                //txtURL.setText(newValue);
                            }
                        });
                    }
                });

                engine.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {

                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (engine.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    panel,
                                                    (value != null)
                                                            ? engine.getLocation() + "\n" + value.getMessage()
                                                            : engine.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });

                jfxPanel.setScene(new Scene(view));
            }
        });
    }

    public void loadURL(final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String tmp = toURL(url);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                engine.load(tmp);
            }
        });
    }

    private static String toURL(String str) {
        try {
            return new URL(str).toExternalForm();
        } catch (MalformedURLException exception) {
            return null;
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                VersionFinal1 browser = new VersionFinal1();
                browser.setVisible(true);
                browser.loadURL("www.google.com.mx");
            }
        });
    }
}
