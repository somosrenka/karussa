/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rediscliente;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import static javafx.concurrent.Worker.State.FAILED;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JPopupMenu.Separator;
import javax.swing.SwingUtilities;

/**
 *
 * @author rkrd_
 */
public class ejemplo2 extends JFrame {

    JPanel panelPadre = new JPanel(new BorderLayout());
    panelNavegador panelNavegador;
    //WebEngine navegador;
    JPopupMenu raizMenu;
    Separator separado1;
    JMenuItem maximizar;
    JMenuItem minimizar;
    JMenuItem salir;
    SystemTray ST;
    TrayIcon TI;
    Image II;
    //WebView view;

    private void inicilizarComponentes() {
        //crearEscena();

        raizMenu = new JPopupMenu();
        separado1 = new Separator();
        maximizar = new JMenuItem();
        maximizar.setIcon(new ImageIcon(getClass().getResource("/iconos/ic_open_in_new_black_24dp_1x.png")));
        maximizar.setText("Maximizar");
        maximizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                maximizarAction(e);
                //loadURL("http://www.google.com.mx");
            }
        });
        minimizar = new JMenuItem();
        salir = new JMenuItem();
        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_exit_to_app_black_24dp_1x.png")));
        salir.setText("salir");
        salir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salirAction(e);
            }
        });
        raizMenu.add(maximizar);
        raizMenu.add(salir);
        setPreferredSize(new Dimension(1024, 600));
        addWindowListener(new WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        GroupLayout Layout = new GroupLayout(getContentPane());
        Layout.setHorizontalGroup(
                Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 500, Short.MAX_VALUE)
        );
        pack();
    }

    private void maximizarAction(ActionEvent e) {

        /*
        Component[] componentes = panelPadre.getComponents();
        System.out.println(componentes.length);
        for (Component component1 : componentes) {
            System.out.println(component1.getName());
            System.out.println(component1.getClass());
            System.out.println(component1.isVisible());
            System.out.println(component1.isDisplayable());
            System.out.println("");
        }
        componentes = panelNavegador.getComponents();
        System.out.println(panelNavegador.getLocation());
        System.out.println(panelNavegador.getSize());
        System.out.println(navegador.getLocation());
        System.out.println(view.getEngine());

        for (Component component1 : componentes) {
            System.out.println(component1.getName());
            System.out.println(component1.getClass());
            System.out.println(component1.isVisible());
            System.out.println(component1.isDisplayable());
            System.out.println("");
        }*/
 /*
        panelNavegador= new JFXPanel();
        crearEscena();
        panelPadre.setVisible(true);
        panelNavegador.setVisible(true);
        view.setVisible(true);
        toFront();
        setVisible(true);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        view.getEngine().load("http://www.google.com.mx");
                    }
                });
            }
        });
        
        --------------------------
        panelPadre= new JPanel();
        setPreferredSize(new Dimension(1024, 600));
        GroupLayout Layout = new GroupLayout(getContentPane());
        Layout.setHorizontalGroup(
                Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 500, Short.MAX_VALUE)
        );
        getContentPane().add(panelPadre);
        
        setVisible(true);
        toFront();
        correr("www.google.com.mx");
        -------------------------
        
         */
        //inicilizarComponentes();
        //navegador.reload();
        ST.remove(TI);
        setVisible(true);
        toFront();
        correr("http://www.google.com.mx");
        /*
        crearEscena();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                loadURL("http://www.google.com.mx");
            }
        });
        panelPadre.remove(panelNavegador);
        getContentPane().remove(panelPadre);
        this.panelNavegador = new JFXPanel();
        this.crearEscena();
        panelPadre.add(panelNavegador, BorderLayout.CENTER);
        getContentPane().add(panelPadre);
        System.out.println("cargando url");
        crearEscena();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            view.getEngine().load("http://www.google.com.mx");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    }
                }
                );

            }
        });

         */
 /*
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("entrando a cargar url");
                String tmp = toURL(url);
                System.out.println(tmp);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                System.out.println("cargando url");
                navegador.load(tmp);
                System.out.println("saliendo");
            }
        });*/
        System.out.println("termino de cargando url");

    }

    private void salirAction(ActionEvent e) {
        System.exit(0);
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {
        // TODO add your handling code here:
        setVisible(false);
        try {
            ST.add(TI);
        } catch (AWTException ex) {
            Logger.getLogger(systemTray.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    private void crearEscena() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                view = new WebView();
                navegador = view.getEngine();
                navegador.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {
                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (navegador.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    panelPadre,
                                                    (value != null)
                                                            ? navegador.getLocation() + "\n" + value.getMessage()
                                                            : navegador.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });
                panelNavegador.setScene(new Scene(view));
            }
        });
    }
    
    public void loadURL(String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("entrando a cargar url");
                String tmp = toURL(url);
                System.out.println(tmp);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                navegador.load(tmp);
            }
        });
    }
    
    private static String toURL(String str) {
        try {
            return new URL(str).toExternalForm();
        } catch (MalformedURLException exception) {
            System.out.println(exception);
            return null;
        }
    }
     */
    public ejemplo2() {
        super();
        inicilizarComponentes();
        if (SystemTray.isSupported()) {
            ST = SystemTray.getSystemTray();
            II = new ImageIcon(getClass().getResource("/iconos/iconito.png")).getImage();
            TI = new TrayIcon(II, "SystemTray", null);
            maximizar.setText("maximizar");
            salir.setText("salir");
            TI.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent evt) {
                    raizMenu.setLocation(evt.getX(), evt.getY());
                    raizMenu.setInvoker(raizMenu);
                    raizMenu.setVisible(true);
                }
            });
            TI.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TI.displayMessage("MenuItem Maximizar", "MenuItem2 Salir", TrayIcon.MessageType.INFO);
                }
            });
            TI.setImageAutoSize(true);
        } else {
            JOptionPane.showMessageDialog(this, "no furula el traysystem");
        }
        System.out.println("se cargo el system tray");
        setLocationRelativeTo(null);
    }

    public void correr(final String url) {
        this.panelNavegador = null;
        panelNavegador = new panelNavegador(url);
        panelPadre.add(panelNavegador, BorderLayout.CENTER);
        getContentPane().add(panelPadre);
        System.out.println("se agrego el panel navegador en el panel padre");
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                panelNavegador.loadURL(url);
            }
        });
    }

    public static void main(String[] args) {
        ejemplo2 browser = new ejemplo2();
        browser.setVisible(true);
        browser.correr("www.google.com.mx");
    }
}
