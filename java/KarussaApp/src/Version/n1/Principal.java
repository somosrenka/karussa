package Version.n1;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JPopupMenu.Separator;

public class Principal extends JFrame {

    JPanel panelPadre = new JPanel(new BorderLayout());
    //loggerKarussa lk = new loggerKarussa();
    configuracion conf = new configuracion();
    clienteJedis CJ;
    JPopupMenu raizMenu;
    Separator separado1;
    JMenuItem maximizar;
    JMenuItem minimizar;
    JMenuItem salir;
    SystemTray ST;
    TrayIcon TI;
    Image II;
    String IP;
    String Canal;

    private void inicilizarComponentes() {
        //crearEscena();
        IP = conf.DameUrl();
        raizMenu = new JPopupMenu();
        separado1 = new Separator();
        maximizar = new JMenuItem();
        maximizar.setIcon(new ImageIcon(getClass().getResource("/iconos/ic_open_in_new_black_24dp_1x.png")));
        maximizar.setText("Maximizar");
        maximizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ST.remove(TI);
                setVisible(true);
                toFront();
            }
        });

        minimizar = new JMenuItem();
        salir = new JMenuItem();
        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_exit_to_app_black_24dp_1x.png")));
        salir.setText("salir");
        salir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        raizMenu.add(maximizar);
        raizMenu.add(salir);
        setPreferredSize(new Dimension(1024, 600));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        GroupLayout Layout = new GroupLayout(getContentPane());
        Layout.setHorizontalGroup(
                Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 500, Short.MAX_VALUE)
        );
        pack();
    }

    private void maximizarAction(ActionEvent e) {
        ST.remove(TI);
        setVisible(true);
        toFront();
    }

    private void salirAction(ActionEvent e) {
        System.exit(0);
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {
        setVisible(false);
        //lk.nivelINFO("Agregando SystemTray");
        try {
            ST.add(TI);
            //lk.nivelINFO("Se agregando SystemTray bien");
        } catch (AWTException e) {
            //lk.nivelSEVERE("No se agrego SystemTray");
            //lk.nivelSEVERE(e.getMessage() + "/" + e.getLocalizedMessage());

        }
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {
        //lk.nivelINFO("iniciando jedis");
        try {
            iniziarJedis(IP, Canal);
            //lk.nivelINFO("se iniciando jedis");
        } catch (Exception e) {
            //lk.nivelSEVERE("no se pudo iniciar jedis");
            //lk.nivelSEVERE(e.getMessage() + "/" + e.getLocalizedMessage());
        }

    }

    public Principal(int canal) {
        super();
        if (canal < 1) {
            login lo = new login();
            lo.setVisible(true);
            lo.toFront();
            this.dispose();
        } else {
            this.Canal = canal + "";
        }
        inicilizarComponentes();
        if (SystemTray.isSupported()) {
            //lk.nivelINFO("iniciando systemTray");
            ST = SystemTray.getSystemTray();
            II = new ImageIcon(getClass().getResource("/iconos/KARUSSA-02-01.png")).getImage();
            TI = new TrayIcon(II, "KarussApp", null);
            maximizar.setText("maximizar");
            salir.setText("salir");
            TI.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent evt) {
                    raizMenu.setLocation(evt.getX(), evt.getY());
                    raizMenu.setInvoker(raizMenu);
                    raizMenu.setVisible(true);
                }
            });

            TI.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TI.displayMessage("MenuItem Maximizar", "MenuItem2 Salir", TrayIcon.MessageType.INFO);
                }
            });
            TI.setImageAutoSize(true);
        } else {
            JOptionPane.showMessageDialog(this, "no furula el traysystem");
        }
        this.setLocationRelativeTo(null);
    }

    public void iniziarJedis(String ip, String canal) {

        try {
            CJ = new clienteJedis(ip, "karussa|usuario__id:" + canal);
        } catch (Exception e) {
            //lk.nivelSEVERE("Error iniciando Jedis");
            //lk.nivelSEVERE(e.getMessage() + "/" + e.getLocalizedMessage());
        }
    }
    /*
    public static void main(String[] args) {
        Principal browser = new Principal(3);
        browser.setVisible(true);

    }
     */
}
