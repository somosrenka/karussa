from datetime import timedelta

import pymysql

from .base import *

pymysql.install_as_MySQLdb()


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_karussa2',
        'HOST': '162.243.186.32',
        'USER': 'dev',
        'PASSWORD': '=K:6qqnIWy967zw',
        'PORT': '3306',
    },
    'db_videos': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_videos',
        'HOST': '162.243.186.32',
        'USER': 'dev',
        'PASSWORD': 'Rd3nvK$',
        'PORT': '3306',
    }
}

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_karussa.sqlite3'),
    }
}
'''

NOMBRE_LOG = "local"
DIRECCION_BASE = 'http://192.168.0.105:8001/'

DEBUG = True

#########   SETTINGS DE SWAMPDRAGON ##################
# Indispensable
#   Esta configuracion aplica si swampdragon no usa algun sistema de autenticacion o cosas asi
# SWAMP_DRAGON_CONNECTION = ('swampdragon.connections.sockjs_connection.DjangoSubscriberConnection', '/data')
# Esta configuracion ya trae implementado un modelo de usuario para swampdragon
SWAMP_DRAGON_CONNECTION = ('swampdragon_auth.socketconnection.HttpDataConnection', '/data')
'''
DRAGON_URL = 'http://localhost:9999/'
#http://192.168.2.8:8001/
'''
# escuchando en localhost
SWAMP_DRAGON_HOST = 'localhost'
SWAMP_DRAGON_HOST = '0.0.0.0'

# puerto de escucha
SWAMP_DRAGON_PORT = '9999'  # default '9999'
# url en la que va estar escuchando para recibir mensajes y esas cosas

# url en la que va estar escuchando para recibir mensajes y esas cosas
ip_ = '192.168.0.105'

#DRAGON_URL = 'http://192.168.42.5:9999/' #PONER LA IP DE LA MAQUINA QUE ESTA CORRIENDO EL SERVIDOR REDIS y EL ARCHIVO remoto.py
#DRAGON_URL = 'http://192.168.43.238:9999/' #
DRAGON_URL = 'http://'+ip_+':9999/' #

# SWAMPDRAGON_TESTMODE = True

##********      TESTEANDO CELERY
# CELERY SETTINGS
# BROKER_URL = 'redis://'+ ip_ +':6379/0'
# CELERY_RESULT_BACKEND = 'redis://'+ip_+':6379/0'
BROKER_URL = "amqp://guest:guest@" + ip_ + ":5672//"
CELERY_RESULT_BACKEND = 'amqp'
# BROKER_URL = "amqp://metallica_rabbit:123456@localhost:5672/localhost"

# BROKER_URL = 'amqp://guest:guest@localhost:5672//'
# CELERY_ACCEPT_CONTENT = ['json', 'pickle']

CELERY_ACCEPT_CONTENT = ['json']
# CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = 'America/Mexico_City'
# CELERY_IGNORE_RESULT = True

# from kombu import serialization
# serialization.registry._decoders.pop("application/x-python-serialize")

# from kombu.serialization import registry
# registry.enable('pickle')


import djcelery

djcelery.setup_loader()
# logger.info('configuracion local')

KARUSSA_HOST = 'http://localhost:8000'
#   Para tareas periodicas
'''
from celery.schedules import crontab
CELERYBEAT_SCHEDULE = {
    #'add-every-30-seconds': {
    #    'task': 'apps.notificaciones.tasks.scraper_example',
    #    'schedule': crontab(hour=7, minute=30, day_of_week=1),
    #    #'args': (16, 16)
    #},
    'listar-usuarios': {
        'task': 'apps.notificaciones.tasks.testing_celery',
        'schedule': crontab(minute='*/5'),  #Se ejecutara cada 5 minutos la funcion testing_celery
        'args': (2,)
    },
}
'''
STATIC_ROOT = "/home/metallica/Escritorio"
HOST = "192.168.0.105"
