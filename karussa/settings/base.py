import os
from datetime import datetime

import pytz
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
import sys
from unipath import Path

SECRET_KEY = '0yj6p36sgv$-0evxz&ec#07=$hq0z7s&l8&j%+5yt+72p7&@2_'

USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGE_CODE = 'es-MX'

DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

TIME_ZONE = 'America/Mexico_City'

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    # 'material.frontend.context_processors.modules',
    # 'apps.metadatos.middlewares.get_current_path',

)

BASE_DIR = Path(__file__).ancestor(3)

# SECRET_KEY = get_secret('SECRET_KEY')

LOGIN_URL = '/login/'

# ALLOWED_HOSTS = ['somosrenka.com', 'www.somosrenka.com','162.243.186.32']
ALLOWED_HOSTS = ['*', ]

ADMINS = (('Carlos_Alberto', 'lord.rattlehead@hotmail.com'),
          ('Ricardo_Vera', 'isc4.tec@gmail.com'))

MANAGERS = (('Carlos_Alberto', 'lord.rattlehead@hotmail.com'), ("Ricardo_Vera", "isc4.tec@gmail.com"))

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.contenttypes',
)

LOCAL_APPS = (
    'apps.videos',
    'apps.empresa',
    'apps.servicios',
    'apps.notificaciones',
    'apps.usuarios',
    'apps.superuser',
    'apps.logger',
    'apps.estadisticas',
    'apps.demo',
    'apps.catalogos',
    'apps.instaladores'
)

THIRD_PARTY_APPS = (
    'suit',
    'swampdragon',
    'swampdragon_auth',
    'snowpenguin.django.recaptcha2',
    'embed_video',
    'nested_inline',
    'djcelery',
    'localflavor',
)

FIXTURE_DIRS = (
    '/datos_base/',
)

INSTALLED_APPS = THIRD_PARTY_APPS + DJANGO_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.security.SecurityMiddleware',
    # 'django_user_agents.middleware.UserAgentMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'karussa.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'karussa.wsgi.application'

STATIC_ROOT = '/static/'
STATIC_URL = '/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# **************    CONFIGURACION DE CORREOS
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'somosrenka@gmail.com'
EMAIL_HOST_PASSWORD = 'karussa040'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'somosrenka@gmail.com'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

AUTH_USER_MODEL = 'usuarios.Usuario'
NOMBRE_LOG = "local"


def formato_fecha(fecha):
    hoy = datetime(fecha.year, fecha.month, fecha.day, 12, 0, 0, 0, pytz.UTC)
    return hoy.strftime('%Y-%m-%d')


#'''
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'completo': {
            'format': "[%(asctime)s] %(levelname)s - [%(pathname)s, %(lineno)s], %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },

        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    'handlers': {
        'archivo': {
            'class': 'logging.FileHandler',
            'filename': "./logs/sistema/" + formato_fecha(datetime.today()) + ".log",
            'formatter': 'completo'
        },
        'db': {
            'class': 'apps.logger.models.KarussaLog',
        },

        'consola': {
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'completo'
        }
    },

    'loggers': {
        'produccion': {
            'handlers': ['db', 'archivo'],
            'propagate': True,
            'level': 'DEBUG',
        },

        'staging': {
            'handlers': ['db', 'archivo', 'consola'],
            'propagate': True,
            'level': 'DEBUG',
        },

        'local': {
            'handlers': ['consola'],
            'propagate': True,
            'level': 'DEBUG'
        }
    }
}
#'''

RECAPTCHA_PRIVATE_KEY = '6Lc07RETAAAAAP-VEXzoZzwjb6tlSzbC4bsY77KM'
RECAPTCHA_PUBLIC_KEY = '6Lc07RETAAAAAMHF2Euo1vDHVZM6o90u2e03gB2n'

HOST = "localhost:8000"

CELERY_RESULT_PERSISTENT = True

