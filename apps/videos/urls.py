# -*- encoding: utf-8 -*-

from django.conf.urls import include, url


urlpatterns = [
    # Examples:
    # url(r'^$', 'karussa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^(?P<slug>[\w-]+)/videos/(?P<llaveRandom>[\w-]+)$', 'apps.videos.views.abrirVideo', name='abrir_video'),
    url(r'^videos/stream/(?P<llaveRandom>[\w-]+)$', 'apps.videos.views.stream', name='stream'),
    url(r'^videos/streaming/(?P<nombre_archivo>[\w-]+)$', 'apps.videos.views.streaming', name='streaming'),
    url(r'^streaming/(?P<llave>[\w-]+)$', 'apps.videos.views.streaming_llave', name='streaming_llave'),

]
