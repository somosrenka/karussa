# -*- encoding: utf-8 -*-
from datetime import datetime
from wsgiref.util import FileWrapper

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render

from apps.estadisticas.models import Historial
from apps.logger.funciones import logger
from apps.servicios.models import Integrantes, VideosPaquete
from apps.usuarios.models import Usuario
from apps.videos.funciones import validarGodinez

# import magic
from karussa.settings.local import DIRECCION_BASE


@login_required
def abrirVideo(request, slug, llaveRandom):
    # if request.method == 'post':
    try:
        # if 'id_godinez' in request.POST:
        # if validarGodinez(int(request.POST['id_godinez']), llaveRandom, slug):
        if validarGodinez(int(request.user.id), llaveRandom, slug):
            # video_id = ListaVideos.objects.get(llave=llaveRandom).video.id
            # video = Video.objects.get(id=video_id)
            # logger.info(video)
            # archivo = video.archivo._get_file()
            # url = video.archivo._get_url()
            # logger.info("existio video")
            # file = FileWrapper(archivo)
            # response = HttpResponse(file, content_type='video/mp4')
            # response['Content-Disposition'] = 'inline; filename=%s' % 'video.mp4'
            # streaming_video = response.streaming
            # logger.info("Pudimos obtener el video con with open")
            return render(request, 'video/vista_video.html', {
                'llaveRandom': llaveRandom,
                "host": DIRECCION_BASE
                # 'mime': mime,
            })
    except Exception as e:
        logger.exception(e.args)
    return render(request, 'usuarios/fuera_de_servicio.html', {

    })


@login_required
def stream(request, llaveRandom):
    try:
        referer = request.META.get('HTTP_REFERER')
        logger.info(referer)
    except Exception as e:
        logger.exception(e)
    try:
        usuario = Usuario.objects.get(id=request.user.id)
        if usuario.isGodinez:
            videosPaquete = VideosPaquete.objects.filter(llave=llaveRandom)
            if videosPaquete.exists():
                integrantes = Integrantes.objects.filter(integrante=usuario.email, paquete__activo=True,
                                                         nivel="godinez")
                if integrantes.exists():
                    if videosPaquete[0].paquete == integrantes[0].paquete:
                        hoy = datetime.today()
                        historial = Historial.objects.filter(usuario=usuario, video=videosPaquete[0])
                        if not historial.exists():
                            archivo = videosPaquete[0].video.archivo._get_file()
                            file = FileWrapper(archivo)
                            response = HttpResponse(file, content_type='video/mp4')
                            response['Content-Disposition'] = 'inline; filename=%s' % 'video.mp4'
                            # DESCOMENTAR
                            # Historial.objects.create(usuario=usuario,video=videosPaquete[0])

                            return response
                        else:
                            raise Http404('Ya vio el video')
                    else:
                        raise Http404('No pertenece al mismo paquete')
                else:
                    raise Http404('No existe el godin')
            else:
                raise Http404('No existe paquete')
        raise Http404('No es godin')
    except Exception as e:
        logger.exception(e)
        raise Http404('Nada por aqui')


# FILEWRAPPER OPTIMIZA EL DESPACHO' DE ARCHIVOS MEDIA
def streaming(request, nombre_archivo):
    try:
        logger.info(nombre_archivo)
        logger.info("busqueda por titulo")
        video = VideosPaquete.objects.get(llave=nombre_archivo).video

        archivo = video.archivo._get_file()
        file = FileWrapper(archivo)

        response = HttpResponse(file, content_type='video/mp4')
        response['Content-Disposition'] = 'inline; filename=%s' % 'video.mp4'
        logger.info("Pudimos obtener el video con with open")
        # response = HttpResponse(video.archivo._get_file(), content_type='video/mp4')
        # response['Content-Disposition'] = 'inline; filename=%s' % 'video.mp4'
        return response
    except Exception as e:
        logger.exception(e)
    raise Http404('Nada por aqui')


'''
def downlad(request, nombre_archivo):
    try:
        logger.info(nombre_archivo)
        logger.info("busqueda por titutlo")
        video = Video.objects.get(titulo=nombre_archivo)
        logger.info("existio video")
        logger.info(file_mime_type(video.archivo.url))
        file = FileWrapper(video.archivo._get_file())
        response = HttpResponse(file, content_type='video/mp4')
        response['Content-Disposition'] = 'attachment; filename=my_video.mp4'

        logger.info("Pudimos obtener el video con with open")
        return response

    except Exception as e:
        logger.info(e)
    return response("Nada por aqui")


def file_mime_type(filename):
    try:
        logger.info("file_mime_type")
        m = magic.open(magic.MAGIC_MIME)
        m.load()
        return (m.file(filename))
    except Exception as e:
        logger.info(e)
        return ("")


# este si funciona xD
def mime_2(nombre_archivo):
    mime = magic.Magic(mime=True)
    mime.from_file(nombre_archivo)
    return mime.from_file(nombre_archivo)
    '''


@login_required
def streaming_llave(request, llave):
    user = request.user
    if request.method == "POST":
        if user.isGodinez:
            paquete = Integrantes.objects.filter(integrante=user.email, paquete__activo=True, estado="Aceptado")[
                0].paquete
            videoPaqt = VideosPaquete.objects.get(llave=llave, paquete=paquete)
            historial = Historial.objects.filter(usuario=user, video=videoPaqt)
            if True:
                archivo = videoPaqt.video.archivo._get_file()
                file = FileWrapper(archivo)
                response = HttpResponse(file, content_type='video/webm')
                response['Content-Disposition'] = 'inline; filename=%s' % 'video.webm'
                return render(request, 'video/vista_video.html', {
                    'llaveRandom': videoPaqt.llave,
                    'response': response,
                })
            else:
                raise Http404("ya vio el video largo")
    if user.isGodinez:
        paquete = Integrantes.objects.get(integrante=user.email, paquete__activo=True, estado="Aceptado",
                                          nivel="godinez").paquete
        videoPaqt = VideosPaquete.objects.get(llave=llave, paquete=paquete)
        historial = Historial.objects.filter(usuario=user, video=videoPaqt)
        if not historial.exists():
            archivo = videoPaqt.video.archivo._get_file()
            file = FileWrapper(archivo)
            response = HttpResponse(file, content_type='video/webm')
            response['Content-Disposition'] = 'inline; filename=%s' % 'video.webm'
            return render(request, 'video/vista_video.html', {
                'llaveRandom': videoPaqt.llave,
                'response': response,
            })
        else:
            raise Http404("ya vio el video largo")
    raise Http404("lo sentimos pero usted esta perdido")
