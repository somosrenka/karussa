from django import forms

from  apps.videos.models import Video


class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = (
            'titulo',
            'descripcion',
            'archivo',
        )

        titulo = forms.CharField(
                max_length=50,
                required=True,
                label='Titulo del Video',
                widget=forms.TextInput(
                        attrs={
                            'required': 'true',
                            'class': 'form-control',
                        }
                )
        )
        descripcion = forms.TextInput(
                attrs={
                    'class': 'form-control',
                },
        )
        archivo = forms.FileInput(
                attrs={
                    'required': 'true',
                    'class': 'form-control',
                }
        )

        def __init__(self, *args, **kwargs):
            self.fields['archivo'].label = "Archivo"
            self.fields['descripcion'].label = "Descripcion del Video"
            super(VideoForm, self).__init__(*args, **kwargs)
