# -*- encoding: utf-8 -*-

from django.contrib import admin
from apps.logger.models import Log


@admin.register(Log)
class AdminLog(admin.ModelAdmin):
    pass
