import os

from django.db import models


# Create your models here.
def instaldor_path(instance, filename):
    return "intaladores/{file}".format(file=filename)


class Instalador(models.Model):
    archivo = models.FileField(max_length=1000, upload_to=instaldor_path)
    tipos = (("linux", "linux"), ("MAC", "MAC"), ("windows", "windows"))
    tipo = models.CharField(choices=tipos, max_length=30)
    titulo = models.CharField(max_length=30, default="")
    contador = 0

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo

    @property
    def filename(self):
        return os.path.basename(self.archivo.name)
