# -*- encoding: utf-8 -*-
from datetime import datetime, date, timedelta

from django.core.management.base import BaseCommand

from apps.servicios.models import Paquete
from apps.superuser.correos import correo_manana_inicia_paquete


class Command(BaseCommand):
    help = 'lo sentimos pero se perdio el manual de ayuda'

    def handle(self, *args, **options):
        maniana = date.today() + timedelta(days=1)
        paquetes = Paquete.objects.filter(fecha_inicio=maniana)
        for paquete in paquetes:
            correo_manana_inicia_paquete(paquete, destino=[paquete.empresa.correo, ])
