# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistroEntrada',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('fecha_login', models.DateTimeField(auto_now_add=True)),
                ('salio', models.BooleanField(default=False)),
                ('navegador', models.CharField(max_length=20)),
                ('ip', models.CharField(max_length=20)),
                ('so', models.CharField(max_length=20)),
                ('dispositivo', models.CharField(max_length=20)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
