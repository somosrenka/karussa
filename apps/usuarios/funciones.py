# -*- encoding: utf-8 -*-

from datetime import datetime, date

import pytz

from apps.empresa.funciones import crear_administrativo_empresa
from apps.empresa.models import Administrativos
from apps.estadisticas.models import Historial
from apps.logger.funciones import logger
from apps.servicios.funciones import genera_cadena_aleatorio
from apps.servicios.models import Integrantes
from apps.usuarios.correos import correo_has_sido_asignado_godinez, correo_has_sido_asignado_monitor
from apps.usuarios.estructuras import integranteEstructura, invitacionMonitor, invitacionGodinez
from apps.usuarios.models import Usuario, RegistroEntrada


def cargar_integrante(integrantes, lista):
    for user in integrantes:
        try:
            usuario = Usuario.objects.get(email=user.integrante)
            estructura = integranteEstructura()
            estructura.correo = usuario.email
            ejercicio = ejercicioCompleto(usuario)
            estructura.id = usuario.id
            estructura.activo = user.estado
            if ejercicio:
                estructura.ejercicio = True
                estructura.hora = dameHoraEjercicio(usuario)
                estructura.molestar = True
            else:
                estructura.ejercicio = False
                estructura.hora = False
                estructura.molestar = True
            lista.append(estructura)
        except Exception as e:
            logger.exception(e)
    return lista


def get_godinez(email):
    try:
        invitacion = Integrantes.objects.get(integrante=email)
        estructura = invitacionGodinez()
        estructura.estado = invitacion.estado
        estructura.correo = email
        return estructura
    except Exception as e:
        logger.exception(e.args)
        return False


def get_monitor(email, slug):
    try:
        logger.info(email)
        invitacion = Integrantes.objects.filter(monitor=email, nivel='creador', estado='Aceptado')
        estructura = invitacionMonitor()
        estructura.correo = email
        estructura.estado = invitacion.estado
        estructura.hijos = []
        return estructura
    except Exception as e:
        logger.exception(e.args)
        return False


def validarNivelUsuario(user, nivel):
    try:
        # empresa = Empresa.objects.get(slug=slug)
        usuario = Usuario.objects.get(id=user.id)
        integrante = Integrantes.objects.get(integrante=usuario.email, nivel=nivel, estado='Aceptado')
        # Administrativos.objects.get(empresa=empresa, monitor=usuario, estado=nivel)
        return True
    except Exception as e:
        logger.exception(e.args)
        return False


def agregar_monitores(padre, nuevos_monitores, paquete):
    for monitor in nuevos_monitores:
        try:
            if Integrantes.objects.filter(paquete=paquete, nivel='monitor').count() >= paquete.monitores:
                # Cantidad de monitores maximo
                return False
            username = str(monitor).strip()
            if username != "":
                email = username
                monitorExiste = False
                if Usuario.objects.filter(email=email).exists():
                    logger.info('existe')
                    nuevo_monitor = Usuario.objects.get(email=email)
                    monitorExiste = Integrantes.objects.filter(
                            paquete=paquete,
                            monitor=padre.email,
                            integrante=nuevo_monitor.email,
                            estado='monitor').exists()
                else:
                    logger.info('nuevo')
                    password = genera_cadena_aleatorio(6)
                    nuevo_monitor = Usuario.objects.create_user(
                            username=username,
                            email=email,
                            password=password
                    )
                    guardar_usuario('monitor', username, password)
                if not monitorExiste:
                    nuevo_monitor.isMonitor = True
                    nuevo_monitor.save()
                    crear_administrativo_empresa(paquete, padre, nuevo_monitor, 'monitor')
                    correo_has_sido_asignado_monitor(password, destino=[email, ])
        except Exception as e:
            logger.exception(e)


def agregar_godinez(nuevos_godinez, empresa, monitor, paquete):
    for godinez in nuevos_godinez:
        try:
            if Integrantes.objects.filter(paquete=paquete, nivel="godinez").count() < paquete.godinez:
                username = str(godinez).strip()
                if username != "":
                    email = username
                    godinezExiste = False
                    if Usuario.objects.filter(email=email).exists():
                        nuevo_godinez = Usuario.objects.get(email=email)
                        godinezExiste = Integrantes.objects.filter(paquete=paquete,
                                                                   integrante=nuevo_godinez.email).exists()
                    else:
                        password = genera_cadena_aleatorio(6)
                        nuevo_godinez = Usuario.objects.create_user(
                                username=username,
                                email=email,
                                password=password
                        )
                        guardar_usuario('godinez', username, password)
                    if not godinezExiste:
                        nuevo_godinez.isGodinez = True
                        nuevo_godinez.save()
                        Integrantes.objects.create(
                                paquete=paquete,
                                integrante=nuevo_godinez.email,
                                monitor=monitor.email,
                                nivel='godinez',
                                estado='Aceptado'
                        )
                        correo_has_sido_asignado_godinez(password, destino=[email, ])
        except Exception as e:
            logger.exception(e)


def guardar_usuario(tipo, username, password):
    documento = open('llaves.txt', 'a')
    documento.write(tipo + " " + username + ":" + password + "\n")
    documento.close()


def ejercicioCompleto(usuario):
    try:
        hoy = date.today()
        x = datetime(hoy.year, hoy.month, hoy.day, 0, 0, 0, 0, pytz.UTC)
        return Historial.objects.filter(usuario=usuario, fecha__gte=x).exists()
    except:
        return False


def dameHoraEjercicio(usuario):
    try:
        hoy = date.today()
        x = datetime(hoy.year, hoy.month, hoy.day, 0, 0, 0, 0, pytz.UTC)
        return Historial.objects.filter(usuario=usuario, fecha__gte=x)[0].fecha
    except:
        return False


def registrar_entrada(usuario):
    # aqui mismo meter los metadatos
    RegistroEntrada.objects.create(
            usuario=usuario
    )


def dame_empresa(usuario, nivel):
    admi = Administrativos.objects.filter(monitor=usuario, estado=nivel)
    if admi.exists():
        if admi.count() == 1:
            return admi[0].empresa
    return False
