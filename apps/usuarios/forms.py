# -*- encoding: utf-8 -*-
from django import forms
from django.conf import settings


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50,
                               required=True,
                               label='Email',
                               widget=forms.TextInput(attrs={
                                   'type': 'email',
                                   'class': 'form-control',
                                   'required': 'true',
                               }))
    password = forms.CharField(max_length=50,
                               required=True,
                               label='Contraseña',
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'class': 'form-control',
                                   'required': 'true',
                               }))

    recordarme = forms.BooleanField(required=False,
                                    widget=forms.CheckboxInput())

    def clean(self):
        cleaned_data = self.cleaned_data
        recordarme = self.cleaned_data.get('recordarme')
        if not recordarme:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        else:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
        return cleaned_data


class cambiarPasswordForm(forms.Form):
    pass



