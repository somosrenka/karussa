# -*- encoding: utf-8 -*-

from django.contrib import admin
from django.contrib.admin.options import TabularInline

from apps.usuarios.models import Usuario, RegistroEntrada


class EntradaSalidaInline(TabularInline):
    model = RegistroEntrada
    extra = 0


@admin.register(Usuario)
class AdminUsuario(admin.ModelAdmin):
    fields = (
        'username',
        'nombre',
        'apellidos',
        'fecha_registro',
        'email',
        'isCreador',
        'isMonitor',
        'isGodinez',
    )
    list_display = (
        'username',
        'email',
        'isCreador',
        'isMonitor',
        'isGodinez',
    )
    readonly_fields = (
        'username',
        'fecha_registro',
        'email',
    )

    inlines = [EntradaSalidaInline, ]
