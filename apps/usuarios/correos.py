# -*- encoding: utf-8 -*-
__author__ = 'metallica'
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from apps.logger.funciones import logger


### Correo de que ha sido asignado, para un usuario final (godinez)
def correo_has_sido_asignado_godinez(password, asunto='Has sido asignado como usuario final | KARUSSA YOGA',
                                     destino=['karussa.yoga@gmail.com', ]):
    try:
        subject = asunto
        from_email = "KARUSSA YOGA <somosrenka@gmail.com>"
        to = destino
        host = settings.HOST
        #   url_accion llevara al usuario a subir un proyecto
        url_accion = host + reverse('usuarios_app:login')
        nivel_usuario = "usuario final"

        ctx = {'nivel_usuario': nivel_usuario,
               'password':password,
               'url_accion': url_accion}

        html_content = render_to_string("correos/correo_has_sido_asignado.html", context=ctx)
        text_content = strip_tags(html_content)

        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    except Exception as e:
        logger.info(e)


### correo de que ha sido asignado, para un monitor
def correo_has_sido_asignado_monitor(password, asunto='Has sido asignado como monitor | KARUSSA YOGA',
                                     destino=['karussa.yoga@gmail.com', ]):
    try:

        subject = asunto
        from_email = "KARUSSA YOGA <somosrenka@gmail.com>"
        to = destino
        host = settings.HOST
        #   url_accion llevara al usuario a subir un proyecto
        url_accion = host + reverse('usuarios_app:login')
        nivel_usuario = "monitor"

        ctx = {'nivel_usuario': nivel_usuario,
               'url_accion': url_accion,
               'password': password}

        html_content = render_to_string("correos/correo_has_sido_asignado.html", context=ctx)
        text_content = strip_tags(html_content)

        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")

        msg.send()
    except Exception as e:
        logger.info(e)
