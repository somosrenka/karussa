# -*- encoding: utf-8 -*-

from django.conf.urls import url

urlpatterns = [
    # Examples:
    # url(r'^$', 'karussa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^monitor$', 'apps.usuarios.views.vista_monitor', name='monitor'),
    url(r'^administrador$', 'apps.usuarios.views.vista_creador', name='creador'),
    url(r'^panel$', 'apps.usuarios.views.vista_godinez', name='godinez'),
    url(r'^administrador/configurar-usuarios$', 'apps.usuarios.views.configurar_usuarios', name='configurar_usuarios'),
    # url(r'^(?P<slug>[\w-]+)/login$', 'apps.usuarios.views.login_personalizado', name='login_personalizado'),
    # url(r'^login/$', 'apps.usuarios.views.login_normal', name='login_global'),
    url(r'^login/$', 'apps.usuarios.views.login_global', name='login'),
    url(r'^loginJava/$', 'apps.usuarios.views.login_java', name='login_java'),
    url(r'^eliminar$', 'apps.usuarios.views.eliminar_usuario', name='eliminar'),
    url(r'^karussa/$', 'apps.superuser.views.superuser', name='superuser'),
    url(r'^karussa/paquetes/crear$', 'apps.superuser.views.crear_paquete', name='crear_paquete'),
    url(r'^change_password$', 'apps.usuarios.views.change_password', name='change_password'),
    url(r'^password_change_done$', 'apps.usuarios.views.password_change_done', name='password_change_done'),

    # url(r'^ajax/agregar/monitor/$', 'apps.usuarios.views.ajax_agregar_monitor', name='ajax_agregar_monitor'),
]
