# -*- encoding: utf-8 -*-

from django.conf.urls import include, url

urlpatterns = [
    # Examples:
    # url(r'^$', 'karussa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^prueba$', 'apps.notificaciones.views.prueba', name='prueba'),
    url(r'^test_karussa$', 'apps.notificaciones.views.test_karussa', name='test_karussa'),
]
