# -*- encoding: utf-8 -*-


from swampdragon import route_handler
from swampdragon.message_format import format_message
from swampdragon.permissions import LoginRequired
from swampdragon.pubsub_providers.model_channel_builder import make_channels
from swampdragon.route_handler import ModelPubRouter, BaseRouter, CHANNEL_DATA_SUBSCRIBE
from apps.empresa.funciones import dame_empresa_por_slug
from apps.empresa.models import Empresa
from apps.notificaciones.funciones import broadcast_sys_info, crear_notificacion_para_usuario
from swampdragon import route_handler
from swampdragon.permissions import LoginRequired

from .models import Notificacion, Notificacion_Version2
from .serializers import NotificacionSerializer, NotificacionVersion2Serializer
from apps.logger.funciones import logger

# ARCHIVO ROUTERS
#   EL router es el que distribuye los mensajes a los usuarios, y se comunica internamente con redis+tornado


from swampdragon.route_handler import ModelPubRouter


class NotificacionesRouter(ModelPubRouter):
    valid_verbs = ['get_list', 'get_single', 'create', 'update', 'delete', 'subscribe', 'unsubscribe',
                   'molestar_usuario']
    # Nombre del router
    route_name = 'karussa_notificaciones'
    model = Notificacion
    serializer_class = NotificacionSerializer

    # FUNCION CUSTOM PARA PUBLICAR
    def custom(self, value):
        logger.info("CUSTOM___")
        logger.info(self)
        logger.info(vars(self))
        # self.publish(['notificacion|*'], {'hello': 'world'})
        self.publish(['notificaciones'], {'hello': 'world'})

    def get_query_set(self, **kwargs):
        logger.info("get_query_set_________________-")
        # logger.info(self)
        # logger.info(kwargs)
        return self.model.objects.filter(user=self.connection.user)

    def get_object(self, **kwargs):
        logger.info("get_object____________________")
        # logger.info(self)
        # logger.info(kwargs)
        # return self.model.objects.get(user=self.connection.user , pk=kwargs['pk'])
        return self.model.objects.get(user=self.connection.user, pk=kwargs['pk'])

    # FUNCION ESPECIAL PARA PUBLICAR EN UN CANAL EN ESPECIFICO
    def get_subscription_channels(self, **kwargs):

        logger.info("____________________GET_SUBSCRIPTION_CHANNELS____________________")
        logger.info('kwargs')
        logger.info(kwargs)
        if self.connection.user is None:
            logger.info("Usuario no logueado")
            return ['anonimo']
            # return self.model.objects.get(user=self.connection.user, pk=kwargs['pk'])
        else:
            return ['notificaciones']
            # return ['usuario']

    def subscribe(self, **kwargs):
        client_channel = kwargs.pop('channel')
        server_channels = make_channels(self.serializer_class, self.include_related,
                                        self.get_subscription_contexts(**kwargs))
        data = self.serializer_class.get_object_map(self.include_related)
        channel_setup = self.make_channel_data(client_channel, server_channels, CHANNEL_DATA_SUBSCRIBE)
        self.send(
            data=data,
            channel_setup=channel_setup,
            **kwargs
        )
        self.connection.pub_sub.subscribe(server_channels, self.connection)

        logger.info("SUBSCRIBE_____")
        logger.info("KWARGS")
        logger.info(kwargs)

        logger.info("SELF_SERIALIZER_CLASS")
        logger.info(vars(self.serializer_class))
        logger.info("SELF._INCLUDE_RELATED")
        logger.info(self.include_related)

        logger.info("CLIENT_CHANNEL")
        logger.info(client_channel)
        logger.info("SERVER_CHANNELS")
        logger.info(server_channels)
        logger.info("DATA")
        logger.info(data)
        logger.info("CHANNEL_SETUP")
        logger.info(channel_setup)
        logger.info("SELF AL FINAL")
        logger.info(vars(self.connection))

    def get_subscription_contexts(self, **kwargs):
        # broadcast_sys_info()
        logger.info("get_subscription_contexts______________________")
        logger.info("" + str(vars(self)))
        logger.info("kwargs__GSC")
        logger.info(kwargs)
        # logger.info(kwargs[])
        logger.info("vars(self.connection)__")
        logger.info(vars(self.connection))
        contexto = dict()

        if 'slug_empresa' in kwargs:
            slug_empresa = kwargs['slug_empresa']
            logger.info(slug_empresa)
            # obtenemos la empresa
            empresa = dame_empresa_por_slug(slug_empresa)
            if empresa is not None:
                logger.info("SI EXISTIO EMPRESA ")
                contexto['empresa__'] = empresa.id

        # Descomentar para que las notificaciones le lleguen solo a un usuario
        #contexto['id_opcional__id'] = self.connection.user.pk
        logger.info("contexto = ",contexto)
        return contexto

    def get_client_context(self, verb, **kwargs):
        logger.info("get_client_context________________________")
        logger.info('CLIENTE_CONTEXT')
        logger.info('KWARGS')
        logger.info(kwargs)
        return {'id__': self.connection.user.pk}

    def send(self, data, channel_setup=None, **kwargs):
        logger.info("metodo send______________________-")
        self.context['state'] = 'success'
        if 'verb' in self.context:
            client_context = self.get_client_context(self.context['verb'], **kwargs)
            self._update_client_context(client_context)
            logger.info("Client_context : " + str(client_context))

        message = format_message(data=data, context=self.context, channel_setup=channel_setup)
        # logger.info("se enviara el mensaje "+str(message))
        self.connection.send(message)
        logger.info("KWARGS en METODO SEND "+str(kwargs))
        logger.info("se ENVIO mensaje "+str(message))



#######     ROUTER PARA VERSION2 DE NOTIFICACIONES
class Notificacionesv2Router(ModelPubRouter):
    valid_verbs = ['get_list','get_single', 'create', 'update', 'delete', 'subscribe', 'unsubscribe', 'molestar_usuario']
    #Nombre del router
    route_name = 'karussa_notificaciones'
    model = Notificacion_Version2
    serializer_class = NotificacionVersion2Serializer

    #FUNCION CUSTOM PARA PUBLICAR
    def custom(self, value):
        logger.info("CUSTOM___")
        logger.info(self)
        logger.info(vars(self))
        #self.publish(['notificacion|*'], {'hello': 'world'})
        self.publish(['notificaciones'], {'hello': 'world'})

    def get_query_set(self, **kwargs):
        logger.info("get_query_set_________________-")
        #logger.info(self)
        #logger.info(kwargs)
        return self.model.objects.filter(user=self.connection.user)

    def get_object(self, **kwargs):
        logger.info("get_object____________________")
        #logger.info(self)
        #logger.info(kwargs)
        #return self.model.objects.get(user=self.connection.user , pk=kwargs['pk'])
        return self.model.objects.get(user=self.connection.user , pk=kwargs['pk'])

    # FUNCION ESPECIAL PARA PUBLICAR EN UN CANAL EN ESPECIFICO
    def get_subscription_channels(self, **kwargs):

        logger.info("GET_SUBSCRIPTION_CHANNELS____________________")
        logger.info(kwargs)
        if self.connection.user is None:
            logger.info("Usuario no logueado")
            return ['anonimo']
            #return self.model.objects.get(user=self.connection.user, pk=kwargs['pk'])
        else:
            return ['notificaciones']
            #return ['usuario']

    def subscribe(self, **kwargs):
        client_channel = kwargs.pop('channel')
        logger.info('antes de make channels')
        server_channels = make_channels(self.serializer_class, self.include_related, self.get_subscription_contexts(**kwargs))
        logger.info('despues de make channels')
        data = self.serializer_class.get_object_map(self.include_related)
        channel_setup = self.make_channel_data(client_channel, server_channels, CHANNEL_DATA_SUBSCRIBE)
        self.send(
            data=data,
            channel_setup=channel_setup,
            **kwargs
        )
        self.connection.pub_sub.subscribe(server_channels, self.connection)

        logger.info("SUBSCRIBE_____")
        logger.info("KWARGS")
        logger.info(kwargs)

        logger.info("SELF_SERIALIZER_CLASS")
        logger.info(vars(self.serializer_class))
        logger.info("SELF._INCLUDE_RELATED")
        logger.info(self.include_related)

        logger.info("CLIENT_CHANNEL")
        logger.info(client_channel)
        logger.info("SERVER_CHANNELS")
        logger.info(server_channels)
        logger.info("DATA")
        logger.info(data)
        logger.info("CHANNEL_SETUP")
        logger.info(channel_setup)
        logger.info("SELF AL FINAL")
        logger.info(vars(self.connection))

    def get_subscription_contexts(self, **kwargs):
        #broadcast_sys_info()
        logger.info("______________________GET_SUBSCRIPTION_CONTEXT______________________")
        logger.info(""+str(vars(self)))
        logger.info("kwargs__GSC")
        logger.info(kwargs)
        #logger.info(kwargs[])
        logger.info("vars(self.connection)__")
        logger.info(vars(self.connection))
        contexto = dict()


        '''
        #DESCOMENTAR PARA QUE SE GENERE UN CANAL PARA CADA EMPRESA
        if 'slug_empresa' in kwargs:
            slug_empresa = kwargs['slug_empresa']
            logger.info(slug_empresa)
            #obtenemos la empresa
            empresa = dame_empresa_por_slug(slug_empresa)
            if empresa is not None:
                logger.info("SI EXISTIO EMPRESA ")
                contexto['empresa__'] = empresa.id
        '''
        # Descomentar para que las notificaciones le lleguen solo a un usuario
        contexto['usuario__id'] = self.connection.user.pk
        logger.info("contexto = ",contexto)
        return contexto

    def get_client_context(self, verb, **kwargs):
        logger.info("get_client_context________________________")
        logger.info('CLIENTE_CONTEXT')
        logger.info('KWARGS')
        logger.info(kwargs)
        return {'id__': self.connection.user.pk}

    def send(self, data, channel_setup=None, **kwargs):
        logger.info("______________________SEND______________________-")
        self.context['state'] = 'success'
        logger.info('kwargs')
        logger.info(kwargs)
        logger.info('self.context')
        logger.info(self.context)

        if 'verb' in self.context:
            client_context = self.get_client_context(self.context['verb'], **kwargs)
            self._update_client_context(client_context)
            logger.info("Client_context : "+str(client_context))

        message = format_message(data=data, context=self.context, channel_setup=channel_setup)
        #logger.info("se enviara el mensaje "+str(message))
        self.connection.send(message)
        logger.info("KWARGS en METODO SEND "+str(kwargs))
        logger.info("se ENVIO mensaje "+str(message))





#route_handler.register(NotificacionesRouter)

route_handler.register(Notificacionesv2Router)




