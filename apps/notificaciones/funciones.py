# -*- encoding: utf-8 -*-
import datetime

from tornado.ioloop import PeriodicCallback

from apps.demo.models import Notification
from apps.empresa.models import Empresa, Administrativos
from apps.notificaciones.models import Notificacion
from apps.usuarios.models import Usuario

#import psutil
import time

__author__ = 'metallica'


pcb = None
tiempo = 15000
#FUNCION QUE SE INVOCARA CADA n mili-segundos (tiempo = )
def broadcast_sys_info():
    global pcb

    if pcb is None:
        pcb = PeriodicCallback(broadcast_sys_info, tiempo )
        pcb.start()

    #cpu = psutil.cpu_percent()
    #net = psutil.net_io_counters()
    #bytes_sent = '{0:.2f} kb'.format(net.bytes_recv / 1024)
    #bytes_rcvd = '{0:.2f} kb'.format(net.bytes_sent / 1024)
    hora = datetime.datetime.now().time()

    print("Son las "+str(hora))

    #crear_notificacion_para_todos()

    #publish_data('notifications',{'data': time.time(),})

segundos = 60
#   ENVIAMOS NOTIFICACIONES CADA CIERTO TIEMPO QUE SE ESPECIFIQUE
def enviar_notificaciones_(minutos = 1):
    print('Inicia enviar_notificaciones_')
    tiempo = minutos*segundos
    activo=True
    while(activo):
        #Dormimos cierto tiempo antes de crear notificaciones
        hora = datetime.datetime.now().time()
        print('se enviaran mensajes a las '+str(hora))
        crear_notificacion_para_todos()
        time.sleep(tiempo)
    return



def crear_notificacion_para_empresa():
    empresa = Empresa.objects.all()[0]
    monitores = empresa.monitores.all()
    print(monitores)
    for monitor in monitores:
        #print(monitor)
        #print(type(monitor))
        Notification.objects.create( message = 'Notificacion AUTOMATICA para '+str(monitor)+'  de la empresa '+str(empresa),
                                     verb = "notificacion_automatica",
                                     user = monitor,
                                     empresa = empresa,
                                     )
        print("creada ")


def crear_notificacion_para_usuario(id_usuario, slug_empresa):
    try:
        empresa = Empresa.objects.get(slug=slug_empresa)
        usuario = Usuario.objects.get(id=id_usuario)

        Notification.objects.create( message = 'Notificacion enviada por el monitor para '+str(usuario)+' de la empresa '+str(empresa),
                                     verb = "notificacion_para_usuario",
                                     user = usuario,
                                     empresa = empresa,
                                     )
        print("creada ")
    except Exception as e:
        print(e)



def crear_notificacion_para_todos():
    administrativos = Administrativos.objects.all()

    for admin in administrativos:
        print(vars(admin))
        Notificacion.objects.create( mensaje = 'Notificacion AUTOMATICA para '+str(admin.monitor)+'  de la empresa '+str(admin.empresa),
                                     id_opcional = admin.monitor.id,
                                     empresa = admin.empresa,
                                     )
        print("creada ")

