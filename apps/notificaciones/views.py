# -*- encoding: utf-8 -*-
from django.contrib.auth.decorators import login_required

from django.shortcuts import render

# Create your views here.
from apps.notificaciones.models import Notificacion, Notificacion_Version2


def prueba(request):
    notificaciones = Notificacion.objects.all()
    return render(request, 'notificaciones/prueba.html', {'usuario': request.user,
                                                           'notificaciones': notificaciones})


@login_required
def test_karussa(request):
    print('test_karussa   ',str(request.user) )
    notificaciones = Notificacion_Version2.objects.filter(usuario=request.user)
    return render(request, 'notificaciones/karusa_notificaciones.html', {'usuario': request.user,
                                                           'notificaciones': notificaciones})




