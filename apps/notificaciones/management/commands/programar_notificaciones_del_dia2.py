# -*- encoding: utf-8 -*-
from datetime import datetime

import pytz
from django.core.management.base import BaseCommand

from apps.notificaciones.tasks import crear_notificaciones_dia
from apps.servicios.funciones import dameVideosDelDia, dameListaGodinez


class Command(BaseCommand):
    help = 'lo sentimos pero se perdio el manual de ayuda'

    def handle(self, *args, **options):
        dia = datetime.today()
        dia = datetime(dia.year, dia.month, dia.day)
        videosDelDia = dameVideosDelDia(dia)
        for video in videosDelDia:
            lista_godinez = dameListaGodinez(video)
            for usuario in lista_godinez:
                dia = datetime.now(pytz.UTC)
                fecha = datetime(year=dia.year, month=dia.month, day=dia.day, hour=video.inicia.hour,
                                 minute=video.inicia.minute, second=1, microsecond=1, tzinfo=pytz.UTC)
                tiempoRestante = ((fecha - dia).seconds)  # - 7200
                if dia < fecha:
                    # crear_notificaciones_dia.apply_async([usuario.id, video.llave], countdown=tiempoRestante)
                    crear_notificaciones_dia.apply_async([usuario.id, video.llave], eta=fecha)

            self.stdout.write('correo mensuales mandados')
