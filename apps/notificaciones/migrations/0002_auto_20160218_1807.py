# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('notificaciones', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notificacion_version2',
            name='empresa',
        ),
        migrations.AddField(
            model_name='notificacion_version2',
            name='creacion',
            field=models.DateTimeField(default=1, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='notificacion_version2',
            name='entregado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='notificacion_version2',
            name='imagen',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='notificacion_version2',
            name='url_video',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='notificacion_version2',
            name='mensaje',
            field=models.TextField(default=''),
        ),
    ]
