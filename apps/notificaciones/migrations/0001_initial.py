# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import swampdragon.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Notificacion',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('mensaje', models.TextField()),
                ('destinatario', models.CharField(default='godinez', choices=[('global', 'global'), ('godinez', 'godinez'), ('monitores', 'monitores'), ('usuario', 'usuario')], max_length=30)),
                ('id_opcional', models.IntegerField(default=0)),
            ],
            bases=(swampdragon.models.SelfPublishModel, models.Model),
        ),
        migrations.CreateModel(
            name='Notificacion_Version2',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('mensaje', models.TextField(default='')),
                ('url_video', models.TextField(default='')),
                ('imagen', models.TextField(default='')),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('entregado', models.BooleanField(default=False)),
                ('destinatario', models.CharField(default='godinez', choices=[('global', 'global'), ('godinez', 'godinez'), ('monitores', 'monitores'), ('usuario', 'usuario')], max_length=30)),
                ('id_opcional', models.IntegerField(default=0)),
            ],
            bases=(swampdragon.models.SelfPublishModel, models.Model),
        ),
    ]
