# -*- encoding: utf-8 -*-
from apps.estadisticas.models import Historial


def funcionCrearHistoalUsuario(Usuario=None, Video=None):
    if Usuario != None and Video != None:
        Historial.objects.create(
            usuario=Usuario,
            video=Video
        )


def funcionUsuarioVideoVisto(Usuario=None, Video=None):
    return Historial.objects.filter(usuario=Usuario, video=Video).exists()
