# -*- encoding: utf-8 -*-
from datetime import date

from django.db import models

from apps.empresa.models import Empresa
from apps.servicios.models import VideosPaquete
from apps.usuarios.models import Usuario


class Historial(models.Model):
    usuario = models.ForeignKey(Usuario)
    video = models.ForeignKey(VideosPaquete)
    fecha = models.DateTimeField(auto_now_add=True)
    postergar = models.IntegerField(default=0)
    esFestivo = models.BooleanField(default=False)

    def __str__(self):
        return self.usuario.nombre + str(self.fecha) + str(self.video.video.titulo)

    def __unicode__(self):
        return self.usuario.nombre + str(self.fecha) + str(self.video.video.descripcion)


class Rating(models.Model):
    usuario = models.ForeignKey(Usuario)
    calificacion = models.IntegerField()
    fecha = models.DateField(default=date.today())
