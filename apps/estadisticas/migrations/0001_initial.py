# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Historial',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('postergar', models.IntegerField(default=0)),
                ('esFestivo', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('calificacion', models.IntegerField()),
                ('fecha', models.DateField(default=datetime.date(2016, 4, 5))),
            ],
        ),
    ]
