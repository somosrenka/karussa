# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demo', '0001_initial'),
        ('empresa', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='empresa',
            field=models.ForeignKey(to='empresa.Empresa', null=True),
        ),
    ]
