from django import forms


class PruebaForm(forms.Form):
    email = forms.CharField(max_length=50,
                            required=True,
                            label='Email de contacto (Requerido)',
                            widget=forms.TextInput(attrs={
                                'type': 'email',
                                'class': 'form-control',
                                'required': 'true',
                            }))

    nombre_empresa = forms.CharField(max_length=100,
                                     min_length=4,
                                     required=False,
                                     label='Nombre de la empresa',
                                     widget=forms.TextInput(attrs={
                                         'class': 'form-control',
                                     }))

    telefono = forms.CharField(max_length=14,
                               min_length=8,
                               required=True,
                               label='Teléfono de contacto (Requerido)',
                               widget=forms.TextInput(attrs={
                                   'class': 'form-control',
                                   'required': 'true',
                               }))
