from django.conf.urls import patterns, include, url
from django.contrib import admin
#from demo.views import Notifications

#admin.autodiscover()

urlpatterns = patterns('',
    #url(r'^$', Notifications.as_view(), name='home'),
    url(r'^demo$', 'apps.demo.views.Notifications', name='home'),
    url(r'^demo_2$', 'apps.demo.views.Notifications_2', name='home_2'),
    url(r'^prueba-validate$', 'apps.demo.views.prueba_validate', name='prueba_validate'),
    #url(r'^admin/', include(admin.site.urls)),
)


