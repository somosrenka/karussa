from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.generic import ListView
from apps.empresa.funciones import login_required_custom
from .models import Notification
from .forms import PruebaForm

'''
class Notifications(ListView):
    model = Notification
    template_name = 'home.html'

    def get_queryset(self):
        logger.info(vars(self))
        logger.info(vars(self.head))
        return self.model.objects.order_by('-pk')
'''

#@login_required_custom
def Notifications(request):
    notificaciones=[]
    if request.user.is_authenticated():
        logger.info("logueado")
        notificaciones =  Notification.objects.filter(user=request.user)
        return render(request,'notificaciones/prueba.html', {'notificaciones':notificaciones,'usuario':request.user})
    else:
        logger.info("anonimo")
        return render(request,'notificaciones/prueba.html', {'notificaciones':[],'usuario':'anonimo'})

def Notifications_2(request):
    notificaciones=[]
    if request.user.is_authenticated():
        logger.info("logueado")
        notificaciones =  Notification.objects.filter(user=request.user)
        return render(request,'notificaciones/prueba.html', {'notificaciones':notificaciones,'usuario':request.user})
    else:
        logger.info("anonimo")
        return render(request,'notificaciones/prueba.html', {'notificaciones':[],'usuario':'anonimo'})


def prueba_validate(request):
    prueba_form = PruebaForm
    if request.method == 'POST':
        prueba_form = PruebaForm(request.POST or None)
    return render_to_response('demo/prueba_validate.html', {'prueba_form': prueba_form})
