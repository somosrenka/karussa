# -*- encoding: utf-8 -*-

from datetime import date

from django.db import models
from django.template.defaultfilters import slugify

from apps.usuarios.models import Usuario


class Empresa(models.Model):
    creador = models.ForeignKey(Usuario)
    nombre = models.CharField(max_length=100)
    slug = models.SlugField(editable=False, max_length=100, null=True)
    # monitores = models.ManyToManyField(Usuario, through='Administrativos', related_name='Administrativos')
    estado = models.TextField()
    ciudad = models.TextField()
    telefono = models.CharField(max_length=16)
    correo = models.EmailField(max_length=100)
    logo = models.ImageField(upload_to='empresas')
    codigo = models.CharField(max_length=10, default="")

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.nombre + "-" + str(date.today()))
        super(Empresa, self).save(*args, **kwargs)

    @property
    def logo_url(self):
        if self.logo and hasattr(self.logo, 'url'):
            return self.logo.url
        else:
            return '/static/base/img/company.png'

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre


class Administrativos(models.Model):
    empresa = models.ForeignKey(Empresa)
    monitor = models.ForeignKey(Usuario)
    estados = (
        ("creador", "creador"),
        ("monitor", "monitor")
    )
    estado = models.CharField(choices=estados, max_length=30)
