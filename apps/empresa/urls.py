# -*- encoding: utf-8 -*-

from django.conf.urls import url

urlpatterns = [
    # Examples:
    # url(r'^$', 'karussa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^logout/$', 'apps.empresa.views.logOut', name='logout'),
    url(r'^perfil/$', 'apps.empresa.views.perfil_empresa', name='perfil'),

]


