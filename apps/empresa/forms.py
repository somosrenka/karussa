# -*- encoding: utf-8 -*-
from django import forms
# from apps.logger.funciones import logger
from apps.usuarios.models import Usuario
from django.utils.translation import ugettext_lazy as _


# Form para autenticar/loguear al usuario
class AutenticacionForm(forms.ModelForm):
    class Meta:
        model = Usuario

        fields = ('email', 'password')
        widgets = {
            'email': forms.EmailInput(attrs={
                'type': 'email',
                'class': 'form-control',
                'required': 'true',
            }),
            'password': forms.TextInput(attrs={
                'type': 'password',
                'class': 'form-control psw',
                'required': 'true',
            }),
        }

        labels = {
            # 'username': _("Nombre de usuario"),
            'email': _("Correo Electronico"),
            'password': _("Contraseña"),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(AutenticacionForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['password'].required = True
