# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Administrativos',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('estado', models.CharField(max_length=30, choices=[('creador', 'creador'), ('monitor', 'monitor')])),
            ],
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('nombre', models.CharField(max_length=100)),
                ('slug', models.SlugField(null=True, max_length=100, editable=False)),
                ('estado', models.TextField()),
                ('ciudad', models.TextField()),
                ('telefono', models.CharField(max_length=16)),
                ('correo', models.EmailField(max_length=100)),
                ('logo', models.ImageField(upload_to='empresas')),
                ('codigo', models.CharField(default='', max_length=10)),
            ],
        ),
    ]
