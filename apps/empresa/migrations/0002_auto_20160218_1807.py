# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):
    dependencies = [
        ('empresa', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='empresa',
            old_name='ubicacion',
            new_name='ciudad',
        ),
        migrations.AddField(
            model_name='empresa',
            name='estado',
            field=models.TextField(default=datetime.datetime(2016, 2, 19, 0, 7, 23, 961729, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
