# -*- encoding: utf-8 -*-
from django.core.mail import EmailMessage
from django.core.mail.message import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.html import strip_tags, format_html

#
from django.shortcuts import render
from karussa.settings.local import DIRECCION_BASE


def view_email(request, email_to):
    subject = "__"
    from_email = "RENKA <somosrenka@gmail.com>"
    to = [email_to, ]

    ctx = {'usuario': request.user,
           'asunto': subject,
           'url_accion': 'https://www.somosrenka.com/socios/perfil/'}

    html_content = render_to_string("correos/correo_bienvenida_usuario.html", context=ctx)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")
    try:
        msg.send()
    except Exception as e:
        pass
        #logger.info("error por " + str(e))

    return render(request, 'correos/email_invitacion_cofundador.html', ctx)


