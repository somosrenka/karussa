# -*- encoding: utf-8 -*-
import json
from datetime import date, timedelta, time

from dateutil.relativedelta import relativedelta

from apps.catalogos.funciones import DIAS
from apps.empresa.models import Empresa
from apps.logger.funciones import logger
from apps.servicios.models import Paquete, Plan, VideosPaquete
from apps.superuser.estructuras import Estructura_plan_json, Estructura_videoPlan_json
from apps.videos.models import Video


def funcion_cargar_json_plan(id_plan):
    estructura = Estructura_plan_json()
    try:
        plan = Plan.objects.get(id=id_plan)
        jsondata = '{"1":[{ "dia":"lunes","video":"1","horaI":"12:00"},{ "dia":"lunes","video":"3","horaI":"2:00"},{ "dia":"Martes","video":"2","horaI":"10:00"},{ "dia":"Martes","video":"1","horaI":"12:00"},{ "dia":"Martes","video":"2","horaI":"2:00"}],"2":[{ "dia":"lunes","video":"1","horaI":"12:00"},{ "dia":"lunes","video":"3","horaI":"2:00"},{ "dia":"Martes","video":"2","horaI":"10:00"},{ "dia":"Martes","video":"1","horaI":"12:00"},{ "dia":"Martes","video":"2","horaI":"2:00"}]}'
        jsondata = '[{"1":[{"dia":"lunes","video":"1","hora":"12:00"},{"dia":"martes","video":"1","hora":"12:00"},{"dia":"miercoles","video":"1","hora":"12:00"},{"dia":"jueves","video":"1","hora":"12:00"},{"dia":"viernes","video":"1","hora":"12:00"}]}'
        # jsonInfo = json.loads(jsondata)
        jsonInfo = json.loads(plan.jsonInfo)
        # logger.info(jsonInfo)
        estructura.id = plan.id
        estructura.titulo = plan.titulo
        for semana in jsonInfo:
            for video in jsonInfo[semana]:
                estructuraVideo = Estructura_videoPlan_json()
                estructuraVideo.dia = video["dia"]
                estructuraVideo.horaInicio = video["horaI"]
                estructuraVideo.video = video["video"]
                estructuraVideo.semana = semana
                estructura.lista_videos.append(estructuraVideo)
        return estructura
    except Exception as e:
        logger.exception(e)


def funcion_paquete_videos(id_empresa, id_plan, jsonDatos="", duracion=0, NumMonitores=0, NumGodinez=0,
                           fechaInicio=date.today()):
    fechaFin = fechaInicio + timedelta(days=duracion)
    datos = '{"1":[{ "dia":"lunes","video":"1","horaI":"12:00"},{ "dia":"lunes","video":"3","horaI":"2:00"},{ "dia":"Martes","video":"2","horaI":"10:00"},{ "dia":"Martes","video":"1","horaI":"12:00"},{ "dia":"Martes","video":"2","horaI":"2:00"}],"2":[{ "dia":"lunes","video":"1","horaI":"12:00"},{ "dia":"lunes","video":"3","horaI":"2:00"},{ "dia":"Martes","video":"2","horaI":"10:00"},{ "dia":"Martes","video":"1","horaI":"12:00"},{ "dia":"Martes","video":"2","horaI":"2:00"}]}'
    jsonDatos = json.loads(jsonDatos)
    logger.info('funcion_paquete_videos')
    logger.info(jsonDatos)
    paquete = Paquete()
    paquete.empresa = Empresa.objects.get(id=id_empresa)
    paquete.plan = Plan.objects.get(id=id_plan)
    paquete.fecha_inicio = fechaInicio
    paquete.fecha_fin = fechaFin
    paquete.jsonInfo = jsonDatos
    paquete.save()
    try:
        lista_semanas = funcion_json_to_array(jsonDatos)
        funcion_array_to_model(lista_semanas, fechaFin, fechaInicio, paquete)
        return "todo salio bien"
    except Exception as e:
        logger.exception(e)
        return "Error: " + e.args


def funcion_json_to_array(jsonDatos):
    jsonDatos = json.loads(jsonDatos)
    logger.info(jsonDatos)
    lista_semanas = []
    for semana in jsonDatos:
        lista_videos = []
        for video in jsonDatos[semana]:
            estructuraVideo = Estructura_videoPlan_json()
            estructuraVideo.dia = video["dia"]
            estructuraVideo.hora = int(str(video["hora"]).split(":")[0])
            estructuraVideo.minu = int(str(video["hora"]).split(":")[1])
            # estructuraVideo.horaInicio = time(hora, minu)
            estructuraVideo.video = video["video"]
            estructuraVideo.semana = semana
            lista_videos.append(estructuraVideo)
        lista_semanas.append(lista_videos)
    return lista_semanas


def funcion_array_to_model(lista_semanas, duracion, fechaInicio, paquete):
    semana = 0
    fecha_anterior = fechaInicio
    temporal = ""
    while duracion > 0:
        dia_semana = DIAS[fechaInicio.strftime('%A')]
        for dia in lista_semanas[semana]:
            '''
            logger.info("dia actual")
            logger.info(dia_semana)
            logger.info(lista_semanas[semana])
            logger.info("objeto")
            logger.info(dia)
            logger.info("dia del objeto")
            logger.info(dia.dia)
            logger.info(duracion)
            '''
            if dia.dia == dia_semana:
                if dia_semana != temporal:
                    temporal = dia_semana
                    duracion -= 1
                try:
                    video = Video.objects.get(id=int(dia.video))
                    VideosPaquete.objects.create(
                            paquete=paquete,
                            video=video,
                            fecha=fechaInicio,
                            inicia=time(dia.hora, dia.minu),
                            fin=time(dia.hora + 1, dia.minu)
                    )
                except Exception as e:
                    logger.info('Error al crear el VideoPaquete')
                    logger.info(e)
                    logger.info(e.args)
                if fecha_anterior != fechaInicio:
                    fecha_anterior = fechaInicio
        if dia_semana == 'Sabado':
            semana += 1
            if semana == lista_semanas.__len__():
                semana = 0
        fechaInicio += timedelta(days=1)
    return fechaInicio


def funcion_crear_plan(titulo, jsoninfo):
    try:
        plan = Plan()
        plan.titulo = titulo
        plan.jsonInfo = jsoninfo
        plan.save()
        return "todo bien :3"
    except Exception as e:
        logger.exception(e)
        return "No se pudo crear el plan por" + e.args
