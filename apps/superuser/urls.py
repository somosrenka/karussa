from django.conf.urls import url

urlpatterns = [
    # Home superuser
    url(r'^$', 'apps.superuser.views.superuser', name='superuser'),

    # Empresas
    url(r'^empresas/$', 'apps.superuser.views.empresas', name='empresas'),
    url(r'^empresas/crear$', 'apps.superuser.views.crear_empresa', name='crear_empresa'),

    # Paquetes
    url(r'^paquetes/$', 'apps.superuser.views.paquetes', name='paquetes'),
    url(r'^paquetes/crear$', 'apps.superuser.views.crear_paquete', name='crear_paquete'),
    url(r'^paquetes/activar/(?P<id_paquete>[0-9]+)$', 'apps.superuser.views.activar_paquete', name='activar_paquete'),
    url(r'^paquetes/borrar/(?P<id_paquete>[0-9]+)$', 'apps.superuser.views.borrar_paquete', name='borrar_paquete'),

    # Planes
    url(r'^planes/crear$', 'apps.superuser.views.crear_plan', name='crear_plan'),

    # URLS para AJAX
    url(r'^planes/ajax-load-plan', 'apps.superuser.views.ajax_load_plan', name='ajax_load_plan'),
]
