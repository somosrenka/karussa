from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from apps.empresa.models import Empresa, Administrativos
from apps.logger.funciones import logger
from apps.servicios.forms import ContactoForm
from apps.servicios.funciones import genera_cadena_aleatorio
from apps.servicios.models import Contacto, Plan, Paquete
from apps.superuser.correos import correo_admin_empresa, correo_paquete_creado
from apps.superuser.forms import CrearEmpresaForm
from apps.superuser.funciones import funcion_json_to_array, funcion_array_to_model
from apps.usuarios.models import Usuario
from apps.videos.forms import VideoForm
from apps.videos.models import Video


@csrf_exempt
@login_required
def crear_paquete(request):
    if request.is_ajax():
        logger.info('crear_paquete')
        logger.info(request.POST)
        logger.info(request.POST['json'])
        empresa = Empresa.objects.get(id=request.POST['empresa'])
        paquete = Paquete.objects.create(
                empresa=empresa,
                fecha_inicio=request.POST['fecha_inicio'],
                jsonInfo=request.POST['json'],
                duracion=request.POST['duracion'],
                godinez=request.POST['cantidad_usuarios'],
                monitores=request.POST['cantidad_monitores']
        )
        #CORREO de paquete creado
        correo_paquete_creado(paquete, destino=[empresa.correo,])
        return HttpResponse(status=200)

    planes = Plan.objects.all()
    videos = Video.objects.all()
    empresas = Empresa.objects.all()
    return render(request, 'superuser/crear_paquete.html',
                  {'planes': planes,
                   'plan_base': planes[0],
                   'videos': videos,
                   'empresas': empresas})


@login_required
@csrf_exempt
def crear_plan(request):
    if request.is_ajax():
        json = request.POST['json']
        logger.info(json)
        Plan.objects.create(titulo=request.POST['nombre'], jsonInfo=json)
        return HttpResponse(status=200)
    videos = Video.objects.all()
    return render(request, 'superuser/crear_plan.html', {'videos': videos})


@login_required
def superuser(request):
    usuario = request.user
    error = ""
    if usuario.is_superuser:
        try:
            '''
            planes_objetos = Plan.objects.filter(visible=True)
            planes = []
            for plan in planes_objetos:
                planes.append(funcion_cargar_plan_estructura(plan))
            '''
            videos = Video.objects.all()
            contactos = Contacto.objects.all()
            form_contacto = ContactoForm()
            form_video = VideoForm()
            return render(request, 'superuser/superuser.html', {
                # 'planesDisponibles': planes,
                'videos': videos,
                'form_contacto': form_contacto,
                'form_video': form_video,
                'contactos': contactos,
            })
        except Exception as e:
            error = e
    return redirect(reverse('servicios_app:home'))
    # raise Http404('fuera no eres super usuario/n' + str(error))


@login_required
@csrf_exempt
def ajax_load_plan(request):
    if request.is_ajax():
        id_plan = request.POST['id_plan'] or None
        plan = Plan.objects.get(id=id_plan)
        return HttpResponse(plan.jsonInfo)


def crear_empresa(request):
    form = CrearEmpresaForm()
    if request.method == 'POST':
        form = CrearEmpresaForm(request.POST, request.FILES)
        if form.is_valid():
            password = genera_cadena_aleatorio(6)
            logger.info("usuario" + str(form.cleaned_data['correo']) + "password" + password)
            try:
                creador = Usuario.objects.get(email=form.cleaned_data['correo'])
            except Exception as e:
                creador = Usuario.objects.create_user(
                        username=form.cleaned_data['correo'],
                        email=form.cleaned_data['correo'],
                        password=password,
                )
                logger.info(e)

            creador.isCreador = True
            creador.save()

            empresa, created = Empresa.objects.update_or_create(
                    creador=creador,
                    nombre=form.cleaned_data['nombre'],
                    correo=form.cleaned_data['correo'],
                    defaults={
                        'estado': form.cleaned_data['estado'],
                        'ciudad': form.cleaned_data['ciudad'],
                        'telefono': form.cleaned_data['telefono'],
                        'logo': form.cleaned_data['logo'],
                    }
            )
            Administrativos.objects.create(
                    empresa=empresa,
                    monitor=creador,
                    estado="creador"
            )

            # Enviar correo al creador para cambiar la pass
            # Checamos si la empresa fue creada
            if created:
                correo_admin_empresa(creador, empresa, password)

            return redirect(reverse('superuser_app:empresas'))
    return render(request, 'superuser/crear_empresa.html', {'form': form})


def empresas(request):
    lista_empresas = Empresa.objects.all()
    for empresa in lista_empresas:
        paquetes_empresa = Paquete.objects.filter(empresa=empresa)
        for paquete in paquetes_empresa:
            if paquete.activo:
                empresa.tiene_paquetes_activos = True
    return render(request, 'superuser/empresas.html', {'empresas': lista_empresas})


def paquetes(request):
    lista_paquetes = Paquete.objects.all()
    return render(request, 'superuser/paquetes.html', {'paquetes': lista_paquetes})

from time import sleep
def activar_paquete(request, id_paquete):
    try:
        logger.info('activar_paquete')
        paquete = Paquete.objects.get(id=id_paquete)
        lista_semanas = funcion_json_to_array(paquete.jsonInfo)
        logger.info(paquete.jsonInfo)
        logger.info(lista_semanas)
        sleep(10)
        paquete.fecha_fin = funcion_array_to_model(lista_semanas, paquete.duracion, paquete.fecha_inicio, paquete)
        paquete.activo = True
        paquete.save()
    except Exception as e:
        logger.info(e)

    return redirect(reverse('superuser_app:paquetes'))


def borrar_paquete(request, id_paquete):
    try:
        paquete = Paquete.objects.get(id=id_paquete)
        paquete.delete()
    except Exception as e:
        logger.info(e)

    return redirect(reverse('superuser_app:paquetes'))
