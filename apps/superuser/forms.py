# -*- encoding: utf-8 -*-
from django import forms
from localflavor.mx.forms import STATE_CHOICES
from django.utils.translation import ugettext_lazy as _

from apps.empresa.models import Empresa


class CrearEmpresaForm(forms.Form):
    nombre = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True
    }))
    correo = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control'
    }))
    estado = forms.ChoiceField(choices=STATE_CHOICES, widget=forms.Select(attrs={
        'class': 'form-control',
        'required': True
    }))
    ciudad = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True
    }))
    telefono = forms.CharField(max_length=20, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True
    }))
    logo = forms.ImageField(required=False, widget=forms.FileInput(attrs={
        'class': 'form-control',
        'required': False
    }))
