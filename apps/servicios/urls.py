# -*- encoding: utf-8 -*-
from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'apps.servicios.views.home', name='home'),
    url(r'^servicios/configurar/$', 'apps.servicios.views.configurar', name='configurar'),
    url(r'^paquetes/configurar/$', 'apps.servicios.views.configurar_paquete', name='configurar_paquete'),
    url(r'^planes/$', 'apps.servicios.views.planes', name='planes'),
    #url(r'^iniciar-paquete/$', 'apps.servicios.views.iniciar_paquete', name='iniciar'),
    #url(r'^actualizar-hora/$', 'apps.servicios.views.actualizar_hora', name='actualizar_hora'),
    url(r'^contacto/$', 'apps.servicios.views.form_contacto', name='contacto'),
    url(r'^planes/finalizar/$', 'apps.servicios.views.finalizar_contacto', name='finalizar_contacto'),
]
