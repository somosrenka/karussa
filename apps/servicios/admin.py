# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.servicios.funciones import validar_pago
from apps.servicios.models import Paquete, Contacto, Integrantes

# admin.site.register(Contacto)

def ActivarServicio(self, request, queryset):
    for q in queryset:
        contacto = Contacto.objects.get(id=q.id)
        validar_pago(contacto)
    return queryset

'''
class VideosDiaInline(admin.TabularInline):
    model = VideosDia
    extra = 0
    fields = ('dia','video', 'llave', 'inicia', 'fin', 'fecha')
    readonly_fields = ('llave',)


@admin.register(Plan)
class AdminPlan(admin.ModelAdmin):
    list_display = (
        'titulo', 'duracion',
        'visible',)
    list_editable = ('visible',)
    fields = ('titulo',
              'duracion', 'cantidad',
              'monitores')
    # readonly_fields = ('titulo', 'duracion')
    inlines = (VideosDiaInline,)


@admin.register(ListaVideos)
class AdminListaVideos(admin.ModelAdmin):
    list_display = ('plan', 'video')
    fields = ('plan', 'video')
    readonly_fields = ('plan', 'video')
'''


@admin.register(Paquete)
class AdminPaquete(admin.ModelAdmin):
    list_display = ('empresa', 'fecha_inicio', 'fecha_fin')
    fields = (
        'empresa', 'plan',
        'fecha_inicio', 'fecha_fin',
        #'monitor', 'pagado',
        'pagado',
        'activo')
    # readonly_fields = ('empresa', 'plan', 'fecha_inicio', 'fecha_fin')


@admin.register(Contacto)
class AdminContacto(admin.ModelAdmin):
    list_display = (
        'email',
        'nombre_contacto',
        'nombre_empresa',
        'fecha_creacion',
        'activo')
    fields = (
        'nombre_contacto',
        'nombre_empresa',
        'telefono',
        'extension',
        'telefono_adicional',
        'email')
    ordering = ['-activo', '-fecha_creacion']

    actions = [ActivarServicio, ]


@admin.register(Integrantes)
class AdminIntegrantes(admin.ModelAdmin):
    pass
