# -*- encoding: utf-8 -*-
from django.core.mail import EmailMessage
from django.core.mail.message import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.html import strip_tags, format_html
from django.shortcuts import render
from apps.logger.funciones import logger
from karussa.settings.local import DIRECCION_BASE


def email_clave_custom_empresa(llave, destino=None):
    if not destino:
        destino = []
    subject = "Tu llave de acceso"
    from_email = "KARUSSA <somosrenka@gmail.com>"
    #
    to = destino
    url_accion = DIRECCION_BASE + reverse('servicios_app:configurar')

    ctx = {'asunto': subject,
           'llave': llave,
           'url_accion': url_accion}

    html_content = render_to_string("correos/correo_clave_custom_empresa.html", context=ctx)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    try:
        msg.send()
    except Exception as e:
        logger.info("error al enviar correo de activar servicio:" + str(e))
        pass
        logger.info("error al enviar correo de activar servicio:" + str(e))


def email_nuevo_contacto(contacto):
    subject = "Nuevo contato de empresa"
    from_email = "KARUSSA <somosrenka@gmail.com>"
    #
    to = ["karussa.yoga@gmail.com"]
    url_accion = DIRECCION_BASE + reverse('superuser_app:superuser')

    ctx = {'asunto': subject,
           'url_accion': url_accion}

    html_content = render_to_string("correos/correo_clave_custom_empresa.html", context=ctx)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")

    msg.send()
    pass