# -*- encoding: utf-8 -*-
import random
import string
from datetime import date, timedelta, time

from dateutil.relativedelta import relativedelta

from apps.logger.funciones import logger
from apps.servicios.correos import email_clave_custom_empresa
from apps.servicios.estructuras import Estructura_plan
from apps.servicios.models import Contacto, Paquete, VideosPaquete, Integrantes
from apps.usuarios.models import Usuario


def activar_paquete(paquete):
    paquete.fecha_fin = paquete.fecha_inicio + relativedelta(months=paquete.plan.duracion)
    logger.info('se actualizo el %s apartir de %s empieza el servicio',
                (str(paquete.plan.titulo) + " " + str(paquete.empresa.nombre), date.today()))
    '''
    aqui hira el correo para el monitor con la informacion necesaria para el siguiente paso
    '''


def funcion_cargar_plan_estructura(plan):
    estructura = Estructura_plan()
    estructura.id = plan.id
    estructura.titulo = plan.titulo
    estructura.duracion = plan.duracion
    estructura.lista_videos = plan.lista
    # estructura.cantidad_videos = str(plan.dame_cantidad_videos())
    estructura.monitores = plan.monitores
    estructura.cantidad = plan.cantidad
    return estructura


def validar_pago(contacto):
    if not contacto.random:
        codigo_prueba = genera_cadena_aleatorio(10)
        while existe_codigo(codigo_prueba):
            codigo_prueba = genera_cadena_aleatorio(10)
        contacto.random = codigo_prueba
        logger.info(codigo_prueba)
        contacto.activo = True
        contacto.save()
        # CORREO enviar llave de acceso
        email_clave_custom_empresa(codigo_prueba, destino=[contacto.email, ])


def genera_cadena_aleatorio(tamanio):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(tamanio))


def existe_codigo(codigo_parametro):
    return Contacto.objects.filter(random=codigo_parametro).exists()


def crear_seleccion_paquete(empresa, plan, creador, hoy):
    paquete = Paquete.objects.create(
            empresa=empresa,
            plan=plan,
            pagado=True,
            # monitor=creador,
            activo=True
    )
    return paquete


def dameVideosDelDia(hoy):
    return VideosPaquete.objects.filter(fecha__lt=hoy + timedelta(days=1), fecha__gte=hoy, paquete__activo=True)


def dameListaGodinez(videoPaquete):
    integrantes = Integrantes.objects.filter(paquete=videoPaquete.paquete, nivel="godinez",
                                             estado="Aceptado").values_list("integrante", flat=True).distinct()
    return Usuario.objects.filter(email__in=integrantes)


'''
def funcionRemplazarPlan(planAntiguo, nombre):
    try:
        nuevoPlan = Plan.objects.get(id=planAntiguo)
        nuevoPlan.pk = None
        nuevoPlan.visible = False
        titulo = nuevoPlan.titulo + " - " + nombre
        nuevoPlan.titulo = titulo
        nuevoPlan.save()
        nuevalistaVideos = VideosDia.objects.filter(plan__id=planAntiguo)
        for lisVideo in nuevalistaVideos:
            lisVideo.id = None
            lisVideo.plan = nuevoPlan
            lisVideo.llave = ''
            lisVideo.fecha = None
            lisVideo.save()
        return str(nuevoPlan.pk)
    except Exception as e:
        logger.exception(e)
        return planAntiguo

def activarFechas(paquete, fecha):
    plan = paquete.plan
    listaVideos = VideosDia.objects.filter(plan=plan)
    cantidad = listaVideos.count()
    listaFechas = CalcularFechas(plan, cantidad, fecha)
    x = 0
    for video in listaVideos:
        video.fecha = listaFechas[x]
        video.save()
        logger.info(video.fecha)
        x += 1
'''


def CalcularFechas(plan, cantidad, fecha):
    listaFechas = []
    while len(listaFechas) < cantidad:
        if fecha.strftime('%A') == 'Monday' and plan.lunes:
            # logger.info('lunes ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Tuesday' and plan.martes:
            # logger.info('martes ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Wednesday' and plan.miercoles:
            # logger.info('miercoles ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Thursday' and plan.jueves:
            # logger.info('jueves ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Friday' and plan.viernes:
            # logger.info('viernes ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Saturday' and plan.sabado:
            # logger.info('sabado ' + str(fecha))
            listaFechas.append(fecha)
        if fecha.strftime('%A') == 'Sunday' and plan.domingo:
            # logger.info('domingo ' + str(fecha))
            listaFechas.append(fecha)
        logger.info(listaFechas)
        fecha += relativedelta(days=1)
    return listaFechas


'''
def asignarHorario(paquete, horaI, horaF):
    plan = paquete.plan
    listaVideos = VideosDia.objects.filter(plan=plan)
    hora1 = time(horaI, 0, 0, 0)
    hora2 = time(horaF, 0, 0, 0)
    for video in listaVideos:
        video.inicia = hora1
        video.fin = hora2
        video.save()
'''


def funcion_dame_contacto(id):
    return Contacto.objects.get(id=id)


def funcion_crear_contacto(POST):
    if 'contactoID' in POST:
        try:
            id_contacto = int(POST['contactoID'])
            if id_contacto > 0:
                contacto = funcion_dame_contacto(id_contacto)
            else:
                contacto = Contacto()
        except Exception as e:
            logger.exception(e)
