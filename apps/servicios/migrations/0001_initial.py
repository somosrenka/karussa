# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('empresa', '0001_initial'),
        ('videos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contacto',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('nombre_contacto', models.CharField(verbose_name='Nombre de contacto', max_length=100, blank=True)),
                ('nombre_empresa', models.CharField(verbose_name='Nombre de Empresa', max_length=100)),
                ('telefono', models.CharField(max_length=20)),
                ('extension', models.CharField(max_length=5)),
                ('telefono_adicional', models.CharField(verbose_name='Teléfono adicional', max_length=20)),
                ('email', models.CharField(max_length=50)),
                ('random', models.CharField(max_length=10)),
                ('activo', models.BooleanField(default=False)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('cantidad_empleados', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Integrantes',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('integrante', models.EmailField(max_length=254)),
                ('monitor', models.EmailField(max_length=254)),
                ('nivel', models.CharField(default='godinez', choices=[('creador', 'creador'), ('monitor', 'monitor'), ('godinez', 'godinez')], max_length=20)),
                ('estado', models.CharField(default='Aceptado', choices=[('Aceptado', 'Aceptado'), ('Eliminado', 'Eliminado'), ('Pendiente', 'Pendiente')], max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Paquete',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('fecha_fin', models.DateField(blank=True, null=True)),
                ('pagado', models.BooleanField(default=False)),
                ('activo', models.BooleanField(default=False)),
                ('duracion', models.IntegerField(default=0)),
                ('jsonInfo', models.TextField()),
                ('monitores', models.IntegerField(default=0)),
                ('godinez', models.IntegerField(default=0)),
                ('empresa', models.ForeignKey(to='empresa.Empresa')),
            ],
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('titulo', models.CharField(max_length=100)),
                ('jsonInfo', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='VideosPaquete',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('dia', models.DateField(default=datetime.date(2016, 4, 5))),
                ('inicia', models.TimeField(blank=True, null=True)),
                ('fin', models.TimeField(blank=True, null=True)),
                ('fecha', models.DateField(blank=True, null=True)),
                ('llave', models.CharField(default='', max_length=20)),
                ('paquete', models.ForeignKey(to='servicios.Paquete')),
                ('video', models.ForeignKey(to='videos.Video')),
            ],
        ),
        migrations.AddField(
            model_name='integrantes',
            name='paquete',
            field=models.ForeignKey(to='servicios.Paquete'),
        ),
        migrations.AddField(
            model_name='contacto',
            name='plan',
            field=models.ForeignKey(to='servicios.Plan'),
        ),
        migrations.CreateModel(
            name='IntegrantesGodinez',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('servicios.integrantes',),
        ),
        migrations.CreateModel(
            name='IntegratesAdministrativos',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('servicios.integrantes',),
        ),
    ]
